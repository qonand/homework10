<?php

namespace App\Http\Controllers;

use Faker\Generator;
use Illuminate\Container\Container;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Queue\Queue;
use Illuminate\Http\Request;
use Illuminate\Queue\QueueManager;
use InvalidArgumentException;

class TestController extends Controller
{
    /**
     * @param string|null $name
     * @return Queue
     */
    private function getConnection(?string $name): Queue
    {
        /** @var QueueManager $connection */
        $queueManager = app('queue');
        switch ($name) {
            case 'beanstalkd':
                return $queueManager->connection('beanstalkd');
            case 'redis':
                return $queueManager->connection('redis');
            case 'redis-aof':
                return $queueManager->connection('redis-aof');

        }

        throw new InvalidArgumentException('Connection not found');
    }

    /**
     * @return Generator
     * @throws BindingResolutionException
     */
    private function getGenerator(): Generator
    {
        return Container::getInstance()->make(Generator::class);
    }

    /**
     * @param Request $request
     * @return array
     * @throws BindingResolutionException
     */
    public function push(Request $request): array
    {
        $connection = $this->getConnection($request->input('connection'));
        $message = serialize([
            'data' => $this->getGenerator()->text(10000)
        ]);

        $connection->pushRaw($message);

        return [
            'success' => true,
            'data' => []
        ];
    }

    /**
     * @param Request $request
     * @return array
     */
    public function pop(Request $request): array
    {
        $connection = $this->getConnection($request->input('connection'));
        $message = $connection->pop();
        $data = [];
        if ($message) {
            $data = $message->getRawBody();
            $message->delete();
        }

        return [
            'success' => true,
            'data' => []
        ];
    }
}
