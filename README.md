# How to install the application

1. Clone the repository `git clone https://qonand@bitbucket.org/qonand/homework2.git`
2. Run `docker-compose up -d nginx beanstalkd redis redis-aof workspace` in `laradock` folder
3. Run `docker-compose exec workspace bash` in `laradock` folder and in opened bash run the following commands:

 - `composer install`
 - `php artisan key:generate`
 - `php artisan migrate`

# How to run the application
1. Run `docker-compose up -d nginx beanstalkd redis redis-aof workspace` in `laradock` folder
2. Open http://localhost/api/test/push?connection={connection}` in your web browser to push a random message to queue
3. Open http://localhost/api/test/pop?connection={connection}` in your web browser to get a message from queue

# Available connection
1. beanstalkd
2. redis
3. redis-aof

# Result of load testing
Beanstalkd
![Scheme](beanstalkd.png)
Redis RDB
![Scheme](redis.png)
Redis AOF
![Scheme](redis-aof.png)